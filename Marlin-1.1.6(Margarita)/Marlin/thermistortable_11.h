/**
 * Marlin 3D Printer Firmware
 * Copyright (C) 2016 MarlinFirmware [https://github.com/MarlinFirmware/Marlin]
 *
 * Based on Sprinter and grbl.
 * Copyright (C) 2011 Camiel Gubbels / Erik van der Zalm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// QU-BD silicone bed QWG-104F-3950 thermistor
const short temptable_11[][2] PROGMEM = {
  {    1 * OVERSAMPLENR, 938 },
  {   31 * OVERSAMPLENR, 314 },
  {   41 * OVERSAMPLENR, 290 },
  {   51 * OVERSAMPLENR, 272 },
  {   61 * OVERSAMPLENR, 258 },
  {   71 * OVERSAMPLENR, 247 },
  {   81 * OVERSAMPLENR, 237 },
  {   91 * OVERSAMPLENR, 229 },
  {  101 * OVERSAMPLENR, 221 },
  {  111 * OVERSAMPLENR, 215 },
  {  121 * OVERSAMPLENR, 209 },
  {  131 * OVERSAMPLENR, 204 },
  {  141 * OVERSAMPLENR, 199 },
  {  151 * OVERSAMPLENR, 195 },
  {  161 * OVERSAMPLENR, 190 },
  {  171 * OVERSAMPLENR, 187 },
  {  181 * OVERSAMPLENR, 183 },
  {  191 * OVERSAMPLENR, 179 },
  {  201 * OVERSAMPLENR, 176 },
  {  221 * OVERSAMPLENR, 170 },
  {  241 * OVERSAMPLENR, 165 },
  {  261 * OVERSAMPLENR, 160 },
  {  281 * OVERSAMPLENR, 155 },
  {  301 * OVERSAMPLENR, 150 },
  {  331 * OVERSAMPLENR, 144 },
  {  340 * OVERSAMPLENR, 139 },
  {  348 * OVERSAMPLENR, 133 },
  {  356 * OVERSAMPLENR, 128 },
  {  386 * OVERSAMPLENR, 123 },
  {  410 * OVERSAMPLENR, 117 },
  {  440 * OVERSAMPLENR, 111 },
  {  470 * OVERSAMPLENR, 105 },
  {  537 * OVERSAMPLENR, 100 },
  {  574 * OVERSAMPLENR,  95 },
  {  612 * OVERSAMPLENR,  90 },
  {  642 * OVERSAMPLENR,  85 },
  {  686 * OVERSAMPLENR,  79 },
  {  737 * OVERSAMPLENR,  72 },
  {  757 * OVERSAMPLENR,  69 },
  {  789 * OVERSAMPLENR,  65 },
  {  833 * OVERSAMPLENR,  57 },
  {  845 * OVERSAMPLENR,  55 },
  {  875 * OVERSAMPLENR,  51 },
  {  902 * OVERSAMPLENR,  45 },
  {  931 * OVERSAMPLENR,  39 },
  {  970 * OVERSAMPLENR,  28 },
  {  977 * OVERSAMPLENR,  23 },
  {  991 * OVERSAMPLENR,  17 },
  { 1001 * OVERSAMPLENR,   9 },
  { 1021 * OVERSAMPLENR, -27 }
};
